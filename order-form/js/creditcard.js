/**
 * Module that contains a couple useful functions for credit card validation
 * and type. Does not require jQuery.
 */
var CREDITCARD = (function (global) {
    var type = {
        MASTERCARD: "Mastercard",
        VISA: "Visa",
        AMERICAN_EXPRESS: "American Express",
        UNKNOWN: "?"
    },

    /**
     * Luhn algorithm in JavaScript: validate credit card number supplied as string of numbers
     * Taken From: https://gist.github.com/ShirtlessKirk/2134376
     * @author ShirtlessKirk. Copyright (c) 2012.
     * @license WTFPL (http://www.wtfpl.net/txt/copying)
     */
    luhnChk = (function (arr) {
        return function (ccNum) {
            var 
                len = ccNum.length,
                bit = 1,
                sum = 0,
                val;

            while (len) {
                val = parseInt(ccNum.charAt(--len), 10);
                sum += (bit ^= 1) ? arr[val] : val;
            }

            return sum && sum % 10 === 0;
        };
    }([0, 2, 4, 6, 8, 1, 3, 5, 7, 9])),

    /**
     * Return the best guess of what the credit card number is
     * given a string
     *
     * @param string - credit card number string
     */
    getType = function (string) {
        if (string && string.length) {
            if (string.charAt(0) === "4") {
                return type.VISA;
            }
            if (string.charAt(0) === "5") {
                return type.MASTERCARD;
            }
            if (string.length > 1) {
                if (string.charAt(0) === "3" &&
                        (string.charAt(1) === "4" ||
                         string.charAt(1) === "7")) {
                    return type.AMERICAN_EXPRESS;
                }
            }
            return "?";
        }
        return "";
    },

    /**
     * Returns whether the credit card string and card type is valid.
     *
     * @param string - credit card number string
     * @param cardType - credit card type
     * @param luhnCheck - use luhn validation, default is false
     */
    isValid = function (string, cardType, luhnCheck) {
        var isNumber = /^\d+$/.test(string),
            cardType = cardType || type.UNKNOWN,
            properLength = false,
            luhnCheck = !!luhnCheck || false;

        if (isNumber) {
            //validate base on CC type
            switch (cardType) {
                case type.VISA:
                    if (string.length >= 13 && string.length <= 16) {
                        properLength = true;
                    }
                    break;

                case type.MASTERCARD:
                    if (string.length === 16) {
                        properLength = true;
                    }
                    break;

                case type.AMERICAN_EXPRESS:
                    if (string.length === 15) {
                        properLength = true;
                    }
                    break;
            }

            if (properLength && luhnCheck) {
                return luhnChk(string);
            }
            return properLength;
        }

        return false;
    },

    /**
     * Return an object with a list of feedback properties
     *
     * @param string - credit card number string
     * @param useLuhnCheck - use Luhn validation, default is false
     */
    getFeedback = function (string, useLuhnCheck) {
        var type = getType(string),
            valid = isValid(string, type, useLuhnCheck);

        return {
            valid: valid,
            type: type
        };
    };

    //expose functions
    return {

        getFeedback: getFeedback,

        // "private" functions, exposed for testing
        _isValid: isValid,
        _getType: getType,

        //expose cc types as well
        _types: type

    };
}(window))
