/**
 * This file mocks out ajax requests that have not yet been implemented.
 */

$.mockjax({
    url: "/payment",
    responseText: "This is an example of a mocked service using mockjax."
});

